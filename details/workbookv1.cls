\ProvidesClass{details/workbookv1}[2025/03/02 V1.2 Андрей Чусов]
\typeout{Учебное пособие ДВФУ}
\NeedsTeXFormat{LaTeX2e}

\ifdraught\PassOptionsToClass{draft}{book}\fi
\LoadClass[oneside]{book}
\RequirePackage[russian]{babel}
\babelprovide[alph=lower, Alph=upper]{russian}
\RequirePackage[sorting=ВКР]{details/bibliography}
\RequirePackage[paper=a4paper]{geometry}
\RequirePackage{fontspec}
\RequirePackage[tableposition=above, figureposition=below]{caption}
\RequirePackage{fancyhdr}
\RequirePackage{titlesec}
\RequirePackage{tocloft}
\RequirePackage{tikz}
\usetikzlibrary{calc}
\RequirePackage{etoolbox}
\RequirePackage{details/counttotal}
\RequirePackage[pdfencoding=auto, hidelinks]{hyperref}
\RequirePackage[numbered, depth=3, openlevel=1]{bookmark}

\geometry{
	paperwidth = 210mm,
	paperheight = 297mm,
	left = 3cm,
	top = 2cm,
	right = 1.5cm,
	bottom = 2cm,
	footskip = 1cm,
	mag = 1000,
	a4paper
}

\input{details/workbook-font-setup.def}
\newcommand{\titlefontsize}{\wb@LARGEfontsize}
\newcommand{\issuefontsize}{\wb@largefontsize}
\newcommand{\issuetypefontsize}{\wb@largefontsize}
\newcommand{\recommendationfontsize}{\wb@normalfontsize}

\newdimen\wb@parindent\wb@parindent1.25cm
\newskip\wb@parskip\wb@parskip\z@ plus1em minus 0.1em
\newcount\wb@secsecvpenalty\wb@secsecvpenalty\@highpenalty

\DeclarePrintbibliographyDefaults{title=Список библиографических источников}

\pagestyle{empty}

\hbadness=10000

\captionsetup[figure]
{
	name={Рис},
	font={small},%
	justification={centering},%
	labelsep={period},%
	labelformat=simple,%
	figurewithin=chapter
}
\DeclareCaptionLabelFormat{wbtable}{\hspace*{\fill}#1 #2}
\captionsetup[table]
{
	name={Таблица},
	textfont={small, bf},%
	labelfont=small,
	labelformat=wbtable,
	justification={centering},%
	labelsep={newline},%
	%labelformat=simple,
	tablewithin=chapter,
	singlelinecheck=false
}

\AtBeginDocument{%
	%algorithm2e redefinition in case it is used
	\def\algocf@captiontext#1#2{Алгоритм \thealgocf{}. \AlCapNameFnt{}#2}
}

%\counterwithout{equation}{section} % undo numbering system provided by phstyle.cls
\counterwithin{equation}{chapter}  % implement desired numbering system

\renewcommand\cfttoctitlefont{\normalfont\normalsize}
\renewcommand\cftchapfont{\normalfont\normalsize}
\renewcommand\cftchappagefont{\normalfont\normalsize}
\renewcommand\cftsecpagefont{\normalfont\normalsize}
\renewcommand\cftsecfont{\normalfont\normalsize}
\setlength\cftbeforetoctitleskip{-1em}
\setlength\cftaftertoctitleskip{-1em}
\setlength\cftbeforechapskip{-1ex}
\setlength\cftbeforesecskip{-1ex}
\renewcommand\cftparskip{6pt}
\renewcommand{\cftchapleader}{\cftdotfill{0}}
\renewcommand{\cftsecleader}{\cftdotfill{0}}
\renewcommand{\cftsubsecleader}{\cftdotfill{0}}
\renewcommand{\cftmarktoc}{}
\setcounter{tocdepth}{1}
%\cftsetindents{section}{2ex}{2ex}
%\cftsetindents{subsection}{\z@}{\z@}
\addtocontents{toc}{\protect\thispagestyle{empty}}

\def\fps@figure{htbp}
\def\fps@table{htbp}
\setcounter{totalnumber}{5}
\setcounter{topnumber}{5}
\renewcommand{\topfraction}{.9}
\renewcommand{\textfraction}{.1}
\renewcommand{\bottomfraction}{.9}
\renewcommand{\floatpagefraction}{0.8}

\interfootnotelinepenalty=\@M

\AtBeginDocument{%
	\renewcommand\contentsname{\hspace{\fill}{\bfseries{}ОГЛАВЛЕНИЕ}\hspace*{\fill}\vspace{1em}\par\nobreak\@afterheading}
	\let\@toc\tableofcontents%
	\renewcommand\tableofcontents{\clearpage\@toc\thispagestyle{empty}\normalsize}
}

\pdfstringdefDisableCommands{%
	\def\XeLaTeX{XeLaTeX}
}

\ExplSyntaxOn
\cs_new_protected:Nn \wb_remove_trailing_period:N {
	\regex_replace_once:nnN {(.*)\.$} {\1} #1
}
\NewDocumentCommand{\removetrailingperiod}{m} {
	\tl_set:Nn \l_tmpa_tl {#1}
	\wb_remove_trailing_period:N \l_tmpa_tl
	\tl_use:N \l_tmpa_tl
}

\cs_new_protected:Nn \wb_correct_quoting:N {\regex_replace_all:nnN {"([^"]*?)"}{«\1»} #1}

\newif\if@wb@aftersection\@wb@aftersectionfalse

\def\wb@addchapter#1{
	{\centering\bfseries#1\par}
	\parindent\wb@parindent
	\nobreak\@afterheading
}

\NewDocumentCommand{\wb@chapter}{som} {
	\tl_set:Nn \l_tmpa_tl {#3}
	\wb_remove_trailing_period:N \l_tmpa_tl
	\wb_correct_quoting:N \l_tmpa_tl
	\str_if_eq:eeTF{\str_foldcase:V \l_tmpa_tl}{\str_foldcase:n{введение}}{\pagestyle{plain}}{
		\str_if_eq:eeT{\str_foldcase:V \l_tmpa_tl}{\str_foldcase:n{предисловие}}{\pagestyle{plain}}}
	\clearpage
	\parindent\z@
	\IfBooleanTF{#1}{
		\tl_set:Nx \l_tmpb_tl {\text_uppercase:n{\tl_use:N \l_tmpa_tl}}
	}{
		\refstepcounter{chapter}
		\tl_set:Nx \l_tmpb_tl {Глава\ \protect{\thechapter}.\ \text_uppercase:n{\tl_use:N \l_tmpa_tl}}
	}
	\wb@addchapter{\tl_use:N \l_tmpb_tl}
	\addcontentsline{toc}{chapter}{\tl_use:N \l_tmpb_tl}
	\@wb@aftersectiontrue\everypar{\@wb@aftersectionfalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\NewDocumentCommand{\wb@appendix}{som} {
	\tl_set:Nn \l_tmpa_tl {#3}
	\wb_remove_trailing_period:N \l_tmpa_tl
	\wb_correct_quoting:N \l_tmpa_tl
	\clearpage
	\parindent\z@
	\IfBooleanTF{#1}{
		\captionsetup*[figure]{figurewithin=none}
		\captionsetup*[table]{tablewithin=none}
		\setcounter{figure}{\z@}
		\setcounter{table}{\z@}
		\wb@addchapter{Приложение\\\tl_use:N \l_tmpa_tl}
		\addcontentsline{toc}{chapter}{Приложение.\ \tl_use:N \l_tmpa_tl}
	}{
		\refstepcounter{appendix}
		\wb@addchapter{Приложение\ \protect{\@Alph\theappendix}\\\tl_use:N \l_tmpa_tl}
		\addcontentsline{toc}{chapter}{Приложение\ \protect{\@Alph\theappendix}.\ \tl_use:N \l_tmpa_tl}
	}
	\@wb@aftersectiontrue\everypar{\@wb@aftersectionfalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\RenewDocumentCommand{\chapter}{}{\wb@chapter}

\NewDocumentCommand{\wb@addsection}{m}{
	{\centering\bfseries#1\par}
	\nobreak\@afterheading
	\parindent\wb@parindent
}

\NewDocumentCommand{\wb@section}{som}{
	\tl_set:Nn \l_tmpa_tl {#3}
	\wb_remove_trailing_period:N \l_tmpa_tl
	\wb_correct_quoting:N \l_tmpa_tl
	\ifhmode\par\fi
	\addvspace{1ex plus 0.5ex minus 0.1ex}
	\parindent\z@
	\if@wb@aftersection\penalty\wb@secsecvpenalty\fi
	\IfBooleanTF {#1} {
		\addcontentsline{toc}{section}{\tl_use:N \l_tmpa_tl}
		\wb@addsection{\tl_use:N \l_tmpa_tl}
	}{
		\refstepcounter{section}
		\addcontentsline{toc}{section}{\protect{\thesection}\ \tl_use:N \l_tmpa_tl}
		\wb@addsection{\protect{\thesection}.\ \tl_use:N \l_tmpa_tl}
	}
	\@wb@aftersectiontrue\everypar{\@wb@aftersectionfalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\RenewDocumentCommand{\section}{}{\wb@section}

\NewDocumentCommand{\wb@addsubsection}{m}{
	{\centering\bfseries\itshape#1\par}
	\nobreak\@afterheading
	\parindent\wb@parindent
}

\NewDocumentCommand{\wb@subsection}{som}{
	\tl_set:Nn \l_tmpa_tl {#3}
	\wb_remove_trailing_period:N \l_tmpa_tl
	\wb_correct_quoting:N \l_tmpa_tl
	\parindent\z@\ifhmode\par\fi
	\addvspace{1ex plus 0.5ex minus 0.1ex}
	\if@wb@aftersection\penalty\wb@secsecvpenalty\fi
	\IfBooleanTF{#1}{
		\addcontentsline{toc}{subsection}{\tl_use:N \l_tmpa_tl}
		\wb@addsubsection{\tl_use:N \l_tmpa_tl}
	}{
		\refstepcounter{subsection}
		\addcontentsline{toc}{subsection}{\protect{\thesubsection}.\ \tl_use:N \l_tmpa_tl}
		\wb@addsubsection{\protect{\thesubsection}.\ \tl_use:N \l_tmpa_tl}
	}
	\@wb@aftersectiontrue\everypar{\@wb@aftersectionfalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\RenewDocumentCommand{\subsection}{}{\wb@subsection}

\NewDocumentCommand{\wb@addsubsubsection}{m}{
	\textbf{#1}
	\vadjust{\nobreak\@afterheading}
}

\NewDocumentCommand{\wb@subsubsection}{som}{
	\tl_set:Nn \l_tmpa_tl {#3}
	\wb_correct_quoting:N \l_tmpa_tl
	\tl_set_eq:NN \l_tmpb_tl \l_tmpa_tl
	\wb_remove_trailing_period:N \l_tmpb_tl
	\ifhmode\par\fi
	\addvspace{0.5em plus 0.5em minus 0.1em}
	\if@wb@aftersection\penalty\wb@secsecvpenalty\fi
	\IfBooleanTF{#1}{
		\addcontentsline{toc}{subsubsection}{\tl_use:N \l_tmpb_tl}
		\wb@addsubsubsection{\tl_use:N \l_tmpa_tl}
	}{
		\refstepcounter{subsubsection}
		\addcontentsline{toc}{subsubsection}{\protect{\thesubsubsection}\ \tl_use:N \l_tmpb_tl}
		\wb@addsubsubsection{\protect{\thesubsubsection}.\ \tl_use:N \l_tmpa_tl}
	}
	\@wb@aftersectiontrue\everypar{\@wb@aftersectionfalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
}

\RenewDocumentCommand{\subsubsection}{}{\wb@subsubsection}

\RenewDocumentCommand{\appendix}{}{\par
	\newcounter{appendix}
	\committotalcounters{}
	\renewcommand{\appendixname}{}
	\gdef\thechapter{\@Alph\c@appendix}
	\renewcommand{\chapter}{\wb@appendix}
}
\ExplSyntaxOff

\newcounter{itemlistctr}
\newcommand{\makenumlabel}[1]{#1.\hfill}
\newenvironment{itemlist}
{
	\begin{list}
    {\arabic{itemlistctr}}
    {\usecounter{itemlistctr}
     \setlength{\labelwidth}{1cm}
     \setlength{\labelsep}{0cm}
     \setlength{\leftmargin}{\z@}
     \setlength{\rightmargin}{\z@}
     \setlength{\itemindent}{2cm}
     \setlength{\itemsep}{\z@}
     \setlength{\parsep}{\z@}
     \setlength{\parskip}{\z@}
     \setlength{\partopsep}{\z@}
     \let\makelabel=\makenumlabel
    }
}
{\end{list}}

\DeclareNameAlias{author}{family-given}
\DeclareFieldFormat{title}{#1}
\DeclareFieldFormat{subtitle}{#1}
\DeclareBibliographyDriver{standard}
{%
	\printfield{title}%
	\newunit
	\printfield{subtitle}%
	\newunit\newblock
	\printfield{type}%
	\newunit\newblock
	\printlist{organization}%
	\newunit
	\printfield{month}, \printfield{year}%
	\finentry
}
\defbibenvironment{bibliography}{\begin{itemlist}}{\end{itemlist}}{\item}%
\renewbibmacro*{url}{\printfield[url]{urlraw}}

\ExplSyntaxOn
\NewDocumentCommand {\authors} {m} {
	\seq_if_empty:NTF {\g_wb_author_name_seq} {} {\ClassError{details/workbookv1}{Authors~of~the~workbook~were~specefied~more~than~once}}
	\clist_set:Nn \l_tmpa_clist {#1}
	\clist_use:Nn \l_tmpa_clist {}
}
\seq_gclear_new:N \g_wb_author_name_seq
\seq_gclear_new:N \g_wb_author_degree_seq
\seq_gclear_new:N \g_wb_author_employment_seq
\seq_gclear_new:N \g_wb_author_affiliation_seq
\cs_new_protected:Nn \wb_normalize_affiliation:N {\regex_replace_once:nnN {^([^,]+),\ *([\w\-А-Яа-я]+)$}{\1\ \(\2\)} #1}
\DeclareDocumentCommand{\author} {m m O{Дальневосточный~федеральный~университет~(Владивосток)} m} {
	\seq_put_right:Nn \g_wb_author_degree_seq{#1}
	\seq_put_right:Nn \g_wb_author_employment_seq{#2}
	\tl_set:Nn \l_tmpa_tl {#3}
	\wb_normalize_affiliation:N \l_tmpa_tl
	\seq_put_right:NV \g_wb_author_affiliation_seq \l_tmpa_tl
	\seq_put_right:Nn \g_wb_author_name_seq {#4}
}
\NewDocumentCommand{\theauthorname}{O{1}} {
	\seq_item:Nn \g_wb_author_name_seq {#1}
}
\NewDocumentCommand{\theauthordegree}{O{1}} {
	\seq_item:Nn \g_wb_author_degree_seq {#1}
}
\NewDocumentCommand{\theauthoremployment} {O{1}} {
	\seq_item:Nn \g_wb_author_employment_seq{#1}
}
\tl_gclear_new:N \g_wb_institute_tl
\NewDocumentCommand{\institute}{m}{\tl_gset:Nn \g_wb_institute_tl {#1}}
\NewDocumentCommand{\theinstitute}{} {\tl_use:N \g_wb_institute_tl}
\tl_gclear_new:N \g_wb_udc_tl
\NewDocumentCommand{\udc}{m}{\tl_gset:Nn \g_wb_udc_tl {#1}}
\NewDocumentCommand{\theudc}{}{\tl_use:N \g_wb_udc_tl}
\tl_gclear_new:N \g_wb_bbc_tl
\NewDocumentCommand{\bbc}{m}{\tl_gset:Nn \g_wb_bbc_tl {#1}}
\NewDocumentCommand{\thebbc}{}{\tl_use:N \g_wb_bbc_tl}
\tl_gclear_new:N \g_wb_amark_tl
\NewDocumentCommand{\amark}{m}{\tl_gset:Nn \g_wb_amark_tl {#1}}
\NewDocumentCommand{\theamark}{}{\tl_use:N \g_wb_amark_tl}
\tl_gclear_new:N \g_wb_isbn_tl
\NewDocumentCommand{\isbn}{m}{\tl_gset:Nn \g_wb_isbn_tl {#1}}
\NewDocumentCommand{\theisbn}{}{\tl_use:N \g_wb_isbn_tl}
\tl_gclear_new:N \g_wb_copyrighttext_tl
\NewDocumentCommand{\copyrighttext}{m}{\tl_gset:Nn \g_wb_copyrighttext_tl {#1}}
\NewDocumentCommand{\thecopyrighttext}{}{\tl_use:N \g_wb_copyrighttext_tl}
\tl_new:N \g_wb_location_tl
\tl_gset:Nn \g_wb_location_tl {Владивосток}
\NewDocumentCommand{\location}{m}{\tl_gset:Nn \g_wb_location_tl {#1}}
\NewDocumentCommand{\thelocation}{}{\tl_use:N \g_wb_location_tl}
\tl_gclear_new:N \g_wb_title_tl
\RenewDocumentCommand{\title}{m}{\tl_gset:Nn \g_wb_title_tl {#1} \wb_correct_quoting:N \g_wb_title_tl}
\NewExpandableDocumentCommand{\thetitle}{}{\tl_use:N \g_wb_title_tl}
\tl_gclear_new:N \g_wb_issue_tl
\NewDocumentCommand{\issue}{m}{\tl_gset:Nn \g_wb_issue_tl {#1}}
\NewDocumentCommand{\theissue}{}{\tl_use:N \g_wb_issue_tl}
\tl_gclear_new:N \g_wb_issuetype_tl
\NewDocumentCommand{\issuetype}{m}{\tl_gset:Nx \g_wb_issuetype_tl {#1}}
\NewDocumentCommand{\theissuetype}{}{\tl_use:N \g_wb_issuetype_tl}
\tl_gclear_new:N \g_wb_recommendation_tl
\NewDocumentCommand{\recommendation}{m}{\tl_gset:Nn \g_wb_recommendation_tl {#1}}
\NewDocumentCommand{\therecommendation}{}{\tl_use:N \g_wb_recommendation_tl}
\seq_gclear_new:N \g_wb_reviewer_name_seq
\seq_gclear_new:N \g_wb_reviewer_degree_seq
\seq_gclear_new:N \g_wb_reviewer_employment_seq
\NewDocumentEnvironment{reviewers}{} {
	\seq_if_empty:NTF \g_wb_reviewer_name_seq {}{\ClassError{details/workbookv1}{Reviewers~are~specified~more~than~once}}
}{}
\NewDocumentCommand{\reviewer}{m m m} {
	\seq_gput_right:Nn \g_wb_reviewer_name_seq {#3}
	\seq_gput_right:Nn \g_wb_reviewer_degree_seq {#1}
	\seq_gput_right:Nn \g_wb_reviewer_employment_seq {#2}
}
\tl_gclear_new:N \g_wb_annotation_tl
\NewDocumentEnvironment{annotation} {+b}{}{\tl_gset:Nn \g_wb_annotation_tl {#1}}
\seq_new:N \g_wb_keywords_ru_seq
\seq_new:N \g_wb_keywords_en_seq
\NewDocumentEnvironment{keywords}{O{ru}+b}{
	\tl_set:Nx \l_tmpa_tl {#1}
	\str_case_e:nnF {\str_foldcase:V{\l_tmpa_tl}} {
		{\str_foldcase:n{ru}}{\seq_gset_split:Nnn\g_wb_keywords_ru_seq {,} {#2}}
		{\str_foldcase:n{en}}{\seq_gset_split:Nnn\g_wb_keywords_en_seq {,} {#2}}
	}{\ClassError{details/workbook}{Unsupported~keywords~language~\str_foldcase:V \l_tmpa_tl}}
}{}
\NewDocumentEnvironment{publisherblock}{}{}{}
\seq_gclear_new:N \g_wb_pub_info_seq
\NewDocumentEnvironment{publisherinfo}{+b}{
	\seq_gput_right:Nn \g_wb_pub_info_seq {#1}
}{}

\NewDocumentCommand{\publicationinfo}{+m}{
	\seq_gput_right:Nn \g_wb_pub_info_seq {#1}
}

\RenewDocumentCommand{\titlepage}{} {
	\setcounter{page}{1}
	\large
	\setlength{\parindent}{\z@}
	\begin{minipage}{\textwidth}
		\sffamily
		\begin{center}
			Дальневосточный~федеральный~университет
			\normalsize\\
			\theinstitute{}
		\end{center}
	\end{minipage}
	\vspace{7em}
	\par
	\begin{minipage}{\textwidth}
	\begin{center}
		\clist_clear:N \l_tmpa_clist
		\seq_map_inline:Nn \g_wb_author_name_seq {
			\clist_put_right:Nn \l_tmpa_clist {\@fullnameinitialslastname{##1}}
		}
		\LARGE
		\clist_use:Nn \l_tmpa_clist {,~}\par
		\vspace{3em}
		\wb@fontsize{\titlefontsize}\bfseries\text_uppercase:n {\thetitle}\normalfont\par
		\tl_if_empty:NTF \g_wb_issue_tl {\vspace{3em}}{\wb@fontsize{\issuefontsize}\vspace{2em}\tl_use:N \g_wb_issue_tl\par}
		\tl_if_empty:NTF \g_wb_issuetype_tl {\vspace{3em}}{\wb@fontsize{\issuetypefontsize}\vspace{1em}\tl_use:N \g_wb_issuetype_tl\par}
		\tl_if_empty:NTF \g_wb_recommendation_tl {} {\wb@fontsize{\recommendationfontsize}\itshape\vspace{2em}Рекомендовано\par\tl_use:N \g_wb_recommendation_tl\normalfont\par}
		\ifdraught
			\vspace{1ex}
			\small
			\int_compare:nNnTF {\seq_count:N \g_wb_author_name_seq} = {1} {
				Я\ подтверждаю
			}{
				Мы\ подтверждаем
			}
			,\ что\ эта\ работа\ является\ оригинальной\ и\ не\ была\ опубликована\ в\ другом\ месте.\ В\ настоящее\ время\ она\ не\ рассматривается\ для\ публикации\ в\ каком-либо\ другом\ издательстве.\\
			\vspace{1em}
			\clist_clear:N \l_tmpa_clist
			\def\wb@tikzline{\tikz\draw[overlay](0, -1pt) -- +(3cm, 0);\hspace{3cm}}
			\seq_map_inline:Nn \g_wb_author_name_seq {
				\clist_put_right:Nn \l_tmpa_clist {\wb@tikzline\nobreakspace/\ \@fullnameinitialslastname{##1}\ /}
			}
			\clist_use:Nn \l_tmpa_clist {,\par\vspace{1ex}}.
		\fi
	\end{center}\end{minipage}
	\vspace*{\fill}
	\begin{center}
		\sffamily\normalsize
		\includegraphics[width=1.8cm, height=2.5cm]{details/leo-logo}\par
		\thelocation\par
		Издательство\ Дальневосточного\ федерального\ университета\par
		\the\year\par
		\normalfont\normalsize
	\end{center}
	\clearpage
}

\NewDocumentEnvironment{copyrightpage}{}{
	\fontsize{10pt}{11pt}\selectfont\setlength{\parindent}{\z@}\sffamily
	\begin{minipage}{\textwidth}
		УДК\ \theudc{}\\
		ББК\ \thebbc{}\\
		\hspace*{\widthof{ББК~}}\theamark
	\end{minipage}
	\par\small
}{
	\vspace{1em}\normalfont\setlength{\parindent}{\wb@parindent}
	Рецензенты:\ 
	\clist_clear:N \l_tmpa_clist
	\int_set:Nn \l_tmpa_int {\seq_count:N \g_wb_reviewer_name_seq}
	\int_set:Nn \l_tmpb_int {1}
	\int_do_while:nn {\l_tmpb_int <= \l_tmpa_int}{
		\clist_clear:N \l_tmpb_clist
		\clist_put_right:Nx \l_tmpb_clist {\exp_not:N \itshape \@fullnameinitialslastname{\seq_item:Nn \g_wb_reviewer_name_seq {\int_use:N \l_tmpb_int}} \exp_not:N \normalfont}
		\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_wb_reviewer_degree_seq {\int_use:N \l_tmpb_int}}
		\tl_if_empty:NTF \l_tmpa_tl {} {
			\abbreviate_degree_short:N \l_tmpa_tl
			\clist_put_right:Nx \l_tmpb_clist {\tl_use:N \l_tmpa_tl	}
		}
		\clist_put_right:Nx \l_tmpb_clist {\seq_item:Nn \g_wb_reviewer_employment_seq {\int_use:N \l_tmpb_int}}
		\clist_put_right:Nx \l_tmpa_clist {{\clist_use:Nn \l_tmpb_clist {,~}}}
		\int_incr:N \l_tmpb_int
	}
	\clist_use:Nn \l_tmpa_clist {;~}.\par
	\vspace{1em minus 1em}
	\begin{center}
		\int_compare:nNnTF {\seq_count:N \g_wb_author_name_seq} < {2} {
			\textit{Автор}\par
			\textbf{\seq_item:Nn \g_wb_author_name_seq {1}},~
			\clist_clear:N \l_tmpa_clist
			\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_wb_author_degree_seq {1}}
			\tl_if_empty:NF \l_tmpa_tl {
				\abbreviate_degree_short:N \l_tmpa_tl
				\clist_put_right:Nx \l_tmpa_clist \l_tmpa_tl
			}
			\clist_put_right:Nx \l_tmpa_clist {\seq_item:Nn \g_wb_author_employment_seq {1}}
			\clist_use:Nn \l_tmpa_clist {,~}\par
			\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_wb_author_affiliation_seq {1}}
			\regex_replace_once:nnNF {^([^\(]*)\(([\w\-А-Яа-я]+)\)$} {\c{textit}{\1}\ \(\2\)} \l_tmpa_tl {
				\ClassWarning{details/workbook1}{Unrecognized~author~affiliation~pattern}
			}
			\tl_use:N \l_tmpa_tl\par
		}{
			\textit{Авторы}\par
			\seq_set_eq:NN \l_tmpa_seq \g_wb_author_affiliation_seq
			\seq_remove_duplicates:N \l_tmpa_seq
			\int_compare:nNnTF {\seq_count:N \l_tmpa_seq} = {1} {
				\int_set:Nn \l_tmpa_int {1}
				\int_set:Nn \l_tmpb_int {\seq_count:N \g_wb_author_name_seq}
				\int_do_while:nn {\l_tmpa_int <= \l_tmpb_int} {
					\textbf{\seq_item:Nn \g_wb_author_name_seq {\int_use:N \l_tmpa_int}}~---~
					\clist_clear:N \l_tmpa_clist
					\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_wb_author_degree_seq {\int_use:N \l_tmpa_int}}
					\tl_if_empty:NF \l_tmpa_tl {
						\abbreviate_degree_short:N \l_tmpa_tl
						\clist_put_right:Nx \l_tmpa_clist \l_tmpa_tl
					}
					\clist_put_right:Nx \l_tmpa_clist {\seq_item:Nn \g_wb_author_employment_seq {\int_use:N \l_tmpa_int}}
					\clist_use:Nn \l_tmpa_clist {,~}\par
					\int_incr:N \l_tmpa_int
				}
				\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_wb_author_affiliation_seq {1}}
				\regex_replace_once:nnNF {^([^\(]*)\(([\w\-А-Яа-я]+)\)$} {\c{textit}{\1}\ \(\2\)} \l_tmpa_tl {
					\ClassWarning{details/workbook1}{Unrecognized~author~affiliation~pattern}
				}
				\tl_use:N \l_tmpa_tl\par
			}{
				\int_set:Nn \l_tmpa_int {1}
				\int_set:Nn \l_tmpb_int {\seq_count:N \g_wb_author_name_seq}
				\int_do_while:nn {\l_tmpa_int <= \l_tmpb_int} {
					\textbf{\seq_item:Nn \g_wb_author_name_seq {\int_use:N \l_tmpa_int}}~---~
					\clist_clear:N \l_tmpa_clist
					\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_wb_author_degree_seq {\int_use:N \l_tmpa_int}}
					\tl_if_empty:NF \l_tmpa_tl {
						\abbreviate_degree_short:N \l_tmpa_tl
						\clist_put_right:Nx \l_tmpa_clist \l_tmpa_tl
					}
					\clist_put_right:Nx \l_tmpa_clist {\seq_item:Nn \g_wb_author_employment_seq {\int_use:N \l_tmpa_int}}
					\clist_use:Nn \l_tmpa_clist {,~}\par
					\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_wb_author_affiliation_seq {\int_use:N \l_tmpa_int}}
					\regex_replace_once:nnNF {^([^\(]*)\(([\w\-А-Яа-я]+)\)$} {\c{textit}{\1}\ \(\2\)} \l_tmpa_tl {
						\ClassWarning{details/workbook1}{Unrecognized~author~affiliation~pattern}
					}
					\tl_use:N \l_tmpa_tl\par
					\int_incr:N \l_tmpa_int
				}
			}
		}
	\end{center}
	\vspace{1em minus 1em}
	\clist_clear:N \l_tmpa_clist
	\seq_map_inline:Nn \g_wb_author_name_seq {
		\tl_set:Nn \l_tmpa_tl {##1}
		\fullname_to_lastname_initials:N \l_tmpa_tl
		\clist_put_right:NV \l_tmpa_clist \l_tmpa_tl
	}
	\setlength{\parindent}{\wb@parindent}\par\textbf{\clist_use:Nn \l_tmpa_clist {,\ }}\ 
	\thetitle{}:~учебное~пособие~для~вузов~/~Политехнический~институт~ДВФУ.
	\tl_if_empty:NF \g_wb_issue_tl {\ --\ \tl_set:No \l_tmpa_tl \g_wb_issue_tl \abbreviate_issue:N\l_tmpa_tl \tl_use:N \l_tmpa_tl}
	\ --\ \tl_set:No \l_tmpa_tl {\g_wb_location_tl :\ Изд-во\ Дальневост.\ федеральн.\ ун-та}
	\tl_use:N \l_tmpa_tl,\ \the\year{}.\ --\ 1\nobreakspace{}CD.\ [\totalpagecount*\ с.].\ --\ Систем.\ требования:\ Acrobat\ Reader,\ Foxit\ Reader\ либо\ другой\ их\ аналог.\ --\ ISBN\ \tl_use:N \g_wb_isbn_tl.\ --\ Текст:\ электронный.\par
	\vspace{1em}\tl_use:N \g_wb_annotation_tl\par
	\seq_if_empty:NF \g_wb_keywords_ru_seq {\itshape Ключевые\ слова:\normalfont\ \seq_use:Nn \g_wb_keywords_ru_seq {,\ }}\par
	\seq_if_empty:NF \g_wb_keywords_en_seq {\itshape Keywords:\normalfont\ \seq_use:Nn \g_wb_keywords_en_seq {,\ }}\par
	\vskip 1em plus 1fill minus 0.5em
	\seq_if_empty:NF \g_wb_pub_info_seq {
		\begin{center}
			\fontsize{10pt}{13.5pt}\selectfont
			\let\oldnl\\
			\def\\{\oldnl\relax}
			\seq_use:Nn \g_wb_pub_info_seq {\par\vspace{1em minus 0.5em}}
			\let\\\oldnl
		\end{center}
	}
	\vspace{1em minus 0.5em}
	\fontsize{10pt}{13pt}\selectfont
	\setlength{\parindent}{\z@}
	\hspace{\fill}Защищено\ от\ копирования \\
	ISBN\ \tl_use:N \g_wb_isbn_tl \hspace{\fill}\copyright{}\ ФГАОУ\ ВО\ \quot{ДВФУ},\ \the\year
	\clearpage
	\normalfont\normalsize
}

\newcounter{wb@assignment} %задачи
\counterwithin{wb@assignment}{chapter}
\NewDocumentCommand{\assignmentlist}{O{Задачи\ для\ самостоятельного\ решения}}{\section*{#1}\setcounter{wb@assignment}{\z@}}
\NewDocumentCommand{\assignment}{+o} {
	\ifhmode\par\fi\parindent\z@
	\addvspace{1ex}
	\if@wb@aftersection\penalty\wb@secsecvpenalty\fi
	\refstepcounter{wb@assignment}
	{\centering\bfseries\itshape{}Задача\ \thewb@assignment\relax\IfValueT{#1}{.\ #1.}\par\nobreak\@afterheading}
	\parindent\wb@parindent
	\@wb@aftersectiontrue\everypar{\@wb@aftersectionfalse\everypar{}}
}

\ExplSyntaxOff
\let\totalprintlists@base\totalprintlists
\renewcommand{\totalprintlists}{\@ifstar{\totalprintlists@base*{250}{2159}}{\totalprintlists@base{250}{2159}}}
\input{details/contractions.def}

\AtBeginDocument{%
	\parskip\wb@parskip
	\parindent\wb@parindent
}
