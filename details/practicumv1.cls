\ProvidesClass{details/practicumv1}[2025/03/02 V0.2 Андрей Чусов]
\typeout{Практикум ДВФУ}
\NeedsTeXFormat{LaTeX2e}

\ifdraught\PassOptionsToClass{draft}{book}\fi
\LoadClass[oneside]{book}
\RequirePackage[russian]{babel}
\babelprovide[alph=lower, Alph=upper]{russian}
\RequirePackage[sortcites,backend=biber,autocite=plain,sorting=nty,giveninits=true]{biblatex}
\RequirePackage[paper=a4paper]{geometry}
\RequirePackage{fontspec}
\RequirePackage[tableposition=above, figureposition=below]{caption}
\RequirePackage{fancyhdr}
\RequirePackage{titlesec}
\RequirePackage{tocloft}
\RequirePackage{tikz}
\usetikzlibrary{calc}
\RequirePackage{etoolbox}
\RequirePackage{details/counttotal}
\RequirePackage[pdfencoding=auto, hidelinks]{hyperref}
\RequirePackage[numbered, depth=3, openlevel=1]{bookmark}

\geometry{
	paperwidth = 210mm,
	paperheight = 297mm,
	left = 3cm,
	top = 2cm,
	right = 1.5cm,
	bottom = 2cm,
	footskip = 1cm,
	mag = 1000,
	a4paper
}

\input{details/workbook-font-setup.def}
\newdimen\titlefontsize\titlefontsize\wb@Largefontsize
\newdimen\subtitlefontsize\subtitlefontsize\wb@largefontsize
\newdimen\audiencefontsize\audiencefontsize\wb@largefontsize
\newdimen\issuefontsize\issuefontsize\wb@largefontsize
\newdimen\issuetypefontsize\issuetypefontsize\wb@normalfontsize
\newdimen\recommendationfontsize\recommendationfontsize\wb@normalfontsize

\newdimen\pct@parindent\pct@parindent1.25cm
\newskip\pct@parskip\pct@parskip\z@ plus1em minus 0.1em
\newcount\pct@secsecvpenalty\pct@secsecvpenalty\@highpenalty

\DeclarePrintbibliographyDefaults{title=Список библиографических источников}

\pagestyle{empty}

\hbadness=10000

\captionsetup[figure]
{
	name={Рис.},
	font={small},%
	justification={centering},%
	labelsep={period},%
	labelformat=simple,%
	%figurewithin=section
}
\DeclareCaptionLabelFormat{wbtable}{\hspace*{\fill}#1 #2}
\captionsetup[table]
{
	name={Таблица},
	textfont={small, bf},%
	labelfont=small,
	labelformat=wbtable,
	justification={centering},%
	labelsep={newline},%
	%labelformat=simple,
	%tablewithin=section,
	singlelinecheck=false
}
\counterwithin*{figure}{section}
\counterwithin*{table}{section}

\AtBeginDocument{%
	%algorithm2e redefinition in case it is used
	\def\algocf@captiontext#1#2{Алгоритм \thealgocf{}. \AlCapNameFnt{}#2}
}

\counterwithin*{equation}{section}  % implement desired numbering system

\renewcommand\cfttoctitlefont{\normalfont\normalsize}
\renewcommand\cftpartfont{\normalfont\normalsize}
\renewcommand\cftpartpagefont{\normalfont\normalsize}
\renewcommand\cftchapfont{\normalfont\normalsize}
\renewcommand\cftchappagefont{\normalfont\normalsize}
\renewcommand\cftsecpagefont{\normalfont\normalsize}
\renewcommand\cftsecfont{\normalfont\normalsize}
\setlength\cftbeforetoctitleskip{-1em}
\setlength\cftaftertoctitleskip{-1em}
\setlength\cftbeforepartskip{-1ex}
\setlength\cftbeforechapskip{-1ex}
\setlength\cftbeforesecskip{-1ex}
\renewcommand\cftparskip{6pt}
\renewcommand\cftsecindent{\z@}
\renewcommand\cftpartnumwidth{\z@}
\renewcommand\cftchapnumwidth{\z@}
\renewcommand\cftsecnumwidth{\z@}
\renewcommand{\cftpartleader}{\cftdotfill{0}}
\renewcommand{\cftchapleader}{\cftdotfill{0}}
\renewcommand{\cftsecleader}{\cftdotfill{0}}
\renewcommand{\cftsubsecleader}{\cftdotfill{0}}
\renewcommand{\cftmarktoc}{}
\setcounter{tocdepth}{1}
%\cftsetindents{section}{2ex}{2ex}
%\cftsetindents{subsection}{\z@}{\z@}
\addtocontents{toc}{\protect\thispagestyle{empty}}

\def\fps@figure{htbp}
\def\fps@table{htbp}
\setcounter{totalnumber}{5}
\setcounter{topnumber}{5}
\renewcommand{\topfraction}{.9}
\renewcommand{\textfraction}{.1}
\renewcommand{\bottomfraction}{.9}
\renewcommand{\floatpagefraction}{0.8}

\interfootnotelinepenalty=\@M

\AtBeginDocument{%
	\renewcommand\contentsname{\hspace{\fill}{\bfseries{}ОГЛАВЛЕНИЕ}\hspace*{\fill}\vspace{1em}\par\nobreak\@afterheading}
	\let\@toc\tableofcontents%
	\renewcommand\tableofcontents{\clearpage\@toc\thispagestyle{empty}\normalsize}
}

\pdfstringdefDisableCommands{%
	\def\XeLaTeX{XeLaTeX}
}

\ExplSyntaxOn
\cs_new_protected:Nn \pct_remove_trailing_period:N {
	\regex_replace_once:nnN {(.*)\.$} {\1} #1
}
\NewDocumentCommand{\removetrailingperiod}{m} {
	\tl_set:Ne \l_tmpa_tl {#1}
	\pct_remove_trailing_period:N \l_tmpa_tl
	\tl_use:N \l_tmpa_tl
}

\cs_new_protected:Nn \pct_correct_quoting:N {\regex_replace_all:nnN {"([^"]*?)"}{«\1»} #1}

\newif\if@pct@afteranysectiontitle\@pct@afteranysectiontitlefalse
\newif\if@pct@afterpartorsection\@pct@afterpartorsectionfalse
\newif\if@pct@insidepart\@pct@insidepartfalse
\newif\if@pct@insidepartorsection\@pct@insidepartorsectionfalse

\newcounter{pct@part}
\newcounter{pct@section}
\counterwithin*{pct@section}{pct@part}
\newcounter{pct@wrk} %номер работы
\counterwithin*{pct@wrk}{pct@section}
\newcounter{pct@subsection}[pct@wrk]
\newcounter{pct@subsubsection}[pct@subsection]
\def\theworknumber{\thepct@wrk}
\let\thelabnumber\theworknumber
\let\thepracticenumber\theworknumber

\def\pct@addpart#1{
	{\centering\bfseries#1\par}
	\parindent\pct@parindent
	\nobreak\@afterheading
}

\NewDocumentCommand{\pct@part}{som} {
	\tl_set:Nn \l_tmpa_tl {#3}
	\pct_remove_trailing_period:N \l_tmpa_tl
	\pct_correct_quoting:N \l_tmpa_tl
	\str_if_eq:eeTF{\str_foldcase:V \l_tmpa_tl}{\str_foldcase:n{введение}}{\pagestyle{plain}}{
		\str_if_eq:eeTF{\str_foldcase:V \l_tmpa_tl}{\str_foldcase:n{предисловие}}{\pagestyle{plain}}{}}
	\clearpage
	\parindent\z@
	\IfBooleanTF{#1}{
		\tl_set:Nx \l_tmpb_tl {\text_uppercase:n{\tl_use:N \l_tmpa_tl}}
	}{
		\refstepcounter{pct@part}
		\tl_set:Nx \l_tmpb_tl {ЧАСТЬ\ \protect{\thepct@part}.\ \text_uppercase:n{\tl_use:N \l_tmpa_tl}}
	}
	\pct@addpart{\tl_use:N \l_tmpb_tl}
	\addcontentsline{toc}{part}{\tl_use:N \l_tmpb_tl}
	\@pct@afteranysectiontitletrue\@pct@afterpartorsectiontrue
	\everypar{\@pct@afteranysectiontitlefalse\@pct@afterpartorsectionfalse\everypar{}}
	\@pct@insideparttrue
	\@pct@insidepartorsectiontrue
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

%\NewDocumentCommand{\pct@appendix}{som} {
%	\tl_set:Nn \l_tmpa_tl {#3}
%	\pct_remove_trailing_period:N \l_tmpa_tl
%	\clearpage
%	\parindent\z@
%	\IfBooleanTF{#1}{
%		\captionsetup*[figure]{figurewithin=none}
%		\captionsetup*[table]{tablewithin=none}
%		\setcounter{figure}{\z@}
%		\setcounter{table}{\z@}
%		\pct@addchapter{Приложение\\\tl_use:N \l_tmpa_tl}
%		\addcontentsline{toc}{chapter}{Приложение.\ \tl_use:N \l_tmpa_tl}
%	}{
%		\refstepcounter{appendix}
%		\pct@addchapter{Приложение\ \protect{\@Alph\theappendix}\\\tl_use:N \l_tmpa_tl}
%		\addcontentsline{toc}{chapter}{Приложение\ \protect{\@Alph\theappendix}.\ \tl_use:N \l_tmpa_tl}
%	}
%	\@pct@aftersectiontrue\everypar{\@pct@aftersectionfalse\everypar{}}
%	\IfValueT{#2}{\label{#2}}
%	\vskip1ex plus 0.5ex minus 0.2ex
%}

\RenewDocumentCommand{\part}{}{\pct@part}

\RenewDocumentCommand{\chapter}{}{\ClassError{details/practicumv1}{Главы\ не\ поддерживаются\ документами\ типа\ "Практикум".}}

\NewDocumentCommand{\pct@addsection}{m}{
	{\centering\bfseries#1\par}
	\nobreak\@afterheading
	\parindent\pct@parindent
}

\NewDocumentCommand{\pct@section}{som}{
	\tl_set:Nn \l_tmpa_tl {#3}
	\pct_correct_quoting:N \l_tmpa_tl
	\pct_remove_trailing_period:N \l_tmpa_tl
	\@pct@insidepartorsectionfalse
	\str_if_eq:eeTF{\str_foldcase:V \l_tmpa_tl}{\str_foldcase:n{введение}}{\pagestyle{plain}}{
		\str_if_eq:eeTF{\str_foldcase:V \l_tmpa_tl}{\str_foldcase:n{предисловие}}{\pagestyle{plain}}{\@pct@insidepartorsectiontrue}}
	\if@pct@afterpartorsection\ifhmode\par\fi\addvspace{1ex plus 0.5ex minus 0.1ex}\penalty\pct@secsecvpenalty\else\clearpage\fi
	\parindent\z@
	\if@pct@insidepart
		\IfBooleanTF {#1} {
			\tl_set_eq:NN \l_tmpb_tl \l_tmpa_tl
		}{
			\refstepcounter{pct@section}
			\tl_set:Nx \l_tmpb_tl {Раздел\ \protect{\thepct@section}.\ \tl_use:N \l_tmpa_tl}
		}
	\else
		\IfBooleanTF {#1} {
			\tl_set:Nx \l_tmpb_tl {\text_uppercase:n{\tl_use:N \l_tmpa_tl}}
		}{
			\refstepcounter{pct@section}
			\tl_set:Nx \l_tmpb_tl {РАЗДЕЛ\ \protect{\thepct@section}.\ \text_uppercase:n{\tl_use:N \l_tmpa_tl}}
		}
	\fi
	\addcontentsline{toc}{chapter}{\tl_use:N \l_tmpb_tl}
	\pct@addsection{\tl_use:N \l_tmpb_tl}
	\@pct@afteranysectiontitletrue\@pct@afterpartorsectiontrue
	\everypar{\@pct@afteranysectiontitlefalse\@pct@afterpartorsectionfalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\RenewDocumentCommand{\section}{}{\pct@section}

\NewDocumentCommand{\work}{oom}{
	\tl_set:Nn \l_tmpa_tl {#3}
	\pct_correct_quoting:N \l_tmpa_tl
	\pct_remove_trailing_period:N \l_tmpa_tl
	\if@pct@afterpartorsection\ifhmode\par\fi\addvspace{1ex plus 0.5ex minus 0.1ex}\penalty\pct@secsecvpenalty\else\clearpage\fi
	\parindent\z@
	\refstepcounter{pct@wrk}
	\IfValueTF{#2} {
		\tl_set:Ne \l_tmpb_tl {#2}
		\regex_replace_once:nnN {\s*$} {\ } \l_tmpb_tl
	}{
		\tl_clear:N \l_tmpb_tl
	}
	\if@pct@insidepartorsection
		\addcontentsline{toc}{section}{\tl_use:N \l_tmpb_tl \protect{\thepct@wrk}.\ \tl_use:N \l_tmpa_tl}
	\else
		\addcontentsline{toc}{section}{\tl_use:N \l_tmpb_tl \protect{\thepct@wrk}.\ \text_uppercase:n{\tl_use:N \l_tmpa_tl}}
	\fi
	{\centering\bfseries \tl_use:N \l_tmpb_tl \protect{\thepct@wrk}\par\vskip1em\text_uppercase:n{\tl_use:N \l_tmpa_tl}\par\vskip1.2em}
	\nobreak\@afterheading
	\parindent\pct@parindent
	\@pct@afteranysectiontitletrue\everypar{\@pct@afteranysectiontitlefalse\everypar{}}
	\IfValueT{#1}{\label{#1}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\NewDocumentCommand{\lab}{om}{
	\work[#1][Лабораторная\ работа\ №]{#2}
}
\NewDocumentCommand{\practice}{om}{
	\work[#1][Практическое\ занятие\ №]{#2}
}

\NewDocumentCommand{\pct@addsubsection}{m}{
	{\centering\bfseries#1\par}
	\nobreak\@afterheading
	\parindent\pct@parindent
}

\NewDocumentCommand{\pct@subsection}{som}{
	\tl_set:Nn \l_tmpa_tl {#3}
	\pct_correct_quoting:N \l_tmpa_tl
	\pct_remove_trailing_period:N \l_tmpa_tl
	\parindent\z@\ifhmode\par\fi
	\addvspace{1ex plus 0.5ex minus 0.1ex}
	\if@pct@afteranysectiontitle\penalty\pct@secsecvpenalty\fi
	\IfBooleanTF{#1}{
		\addcontentsline{toc}{subsection}{\tl_use:N \l_tmpa_tl}
		\pct@addsubsection{\tl_use:N \l_tmpa_tl}
	}{
		\refstepcounter{pct@subsection}
		\addcontentsline{toc}{subsection}{\protect{\thepct@subsection}.\ \tl_use:N \l_tmpa_tl}
		\pct@addsubsection{\protect{\thepct@subsection}.\ \tl_use:N \l_tmpa_tl}
	}
	\@pct@afteranysectiontitletrue\everypar{\@pct@afteranysectiontitlefalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\RenewDocumentCommand{\subsection}{}{\pct@subsection}

\NewDocumentCommand{\pct@addsubsubsection}{m}{
	{\centering\itshape\bfseries#1\par}
	\nobreak\@afterheading
}

\NewDocumentCommand{\pct@subsubsection}{som}{
	\tl_set:Nn \l_tmpa_tl {#3}
	\pct_correct_quoting:N \l_tmpa_tl
	\tl_set_eq:NN \l_tmpb_tl \l_tmpa_tl
	\pct_remove_trailing_period:N \l_tmpb_tl
	\ifhmode\par\fi
	\parindent\z@
	\addvspace{0.5em plus 0.5em minus 0.1em}
	\if@pct@afteranysectiontitle\penalty\pct@secsecvpenalty\fi
	\IfBooleanTF{#1}{
		\addcontentsline{toc}{subsubsection}{\tl_use:N \l_tmpb_tl}
		\pct@addsubsubsection{\tl_use:N \l_tmpa_tl}
	}{
		\refstepcounter{pct@subsubsection}
		\addcontentsline{toc}{subsubsection}{\protect{\thepct@subsubsection}\ \tl_use:N \l_tmpb_tl}
		\pct@addsubsubsection{\protect{\thepct@subsubsection}.\ \tl_use:N \l_tmpa_tl}
	}
	\parindent\pct@parindent
	\@pct@afteranysectiontitletrue\everypar{\@pct@afteranysectiontitlefalse\everypar{}}
	\IfValueT{#2}{\label{#2}}
	\vskip1ex plus 0.5ex minus 0.2ex
}

\RenewDocumentCommand{\subsubsection}{}{\pct@subsubsection}

\NewDocumentCommand{\pct@paragraph}{som}{
	\ifhmode\par\fi
	\if@pct@afteranysectiontitle\@pct@afteranysectiontitlefalse\penalty\pct@secsecvpenalty\everypar{}\else\@afterindenttrue\if@nobreak\everypar{}\else\addvspace{2.5ex plus 1ex minus.2ex}\penalty\@M\fi\fi
	\noindent{\bfseries #3}\@nobreakfalse\clubpenalty\@M\everypar{}
	\hskip1em\ignorespaces
}

\RenewDocumentCommand{\paragraph}{}{\pct@paragraph}

\NewDocumentCommand{\pct@subparagraph}{som}{
	\ifhmode\par\fi
	\if@pct@afteranysectiontitle\@pct@afteranysectiontitlefalse\penalty\pct@secsecvpenalty\everypar{}\else\@afterindenttrue\if@nobreak\everypar{}\else\addvspace{2.5ex plus 1ex minus.2ex}\penalty\@M\fi\fi
	{\bfseries #3}\@nobreakfalse\clubpenalty\@M\everypar{}
	\hskip1em\ignorespaces
}

\RenewDocumentCommand{\subparagraph}{}{\pct@subparagraph}

\RenewDocumentCommand{\appendix}{}{\par
	\newcounter{appendix}
	\committotalcounters{}
	\renewcommand{\appendixname}{}
	\gdef\thechapter{\@Alph\c@appendix}
	\renewcommand{\chapter}{\pct@appendix}
}
\ExplSyntaxOff

\newcounter{itemlistctr}
\newcommand{\makenumlabel}[1]{#1.\hfill}
\newenvironment{itemlist}
{
	\begin{list}
    {\arabic{itemlistctr}}
    {\usecounter{itemlistctr}
     \setlength{\labelwidth}{1cm}
     \setlength{\labelsep}{0cm}
     \setlength{\leftmargin}{\z@}
     \setlength{\rightmargin}{\z@}
     \setlength{\itemindent}{2cm}
     \setlength{\itemsep}{\z@}
     \setlength{\parsep}{\z@}
     \setlength{\parskip}{\z@}
     \setlength{\partopsep}{\z@}
     \let\makelabel=\makenumlabel
    }
}
{\end{list}}

\DeclareNameAlias{author}{family-given}
\DeclareFieldFormat{title}{#1}
\DeclareFieldFormat{subtitle}{#1}
\DeclareBibliographyDriver{standard}
{%
	\printfield{title}%
	\newunit
	\printfield{subtitle}%
	\newunit\newblock
	\printfield{type}%
	\newunit\newblock
	\printlist{organization}%
	\newunit
	\printfield{month}, \printfield{year}%
	\finentry
}
\DeclareSourcemap{
	\maps[datatype=bibtex]{
		\map[overwrite]
		{
			\step[fieldsource=author, match=\regexp{^[А-Яа-я].+}, final]
			\step[fieldset=presort, fieldvalue=1, append]
		}
		\map[overwrite]
		{
			\step[notfield=author, final]
			\step[fieldsource=title, match=\regexp{^[А-Яа-я].+}, final]
			\step[fieldset=presort, fieldvalue=1, append]
		}
		\map[overwrite]
		{
			\step[fieldsource=author, match=\regexp{^[A-Za-z].+}, final]
			\step[fieldset=presort, fieldvalue=2, append]
		}
		\map[overwrite]
		{
			\step[notfield=author, final]
			\step[fieldsource=title, match=\regexp{^[A-Za-z].+}, final]
			\step[fieldset=presort, fieldvalue=2, append]
		}
	}
}
\defbibenvironment{bibliography}{\begin{itemlist}}{\end{itemlist}}{\item}%
\renewbibmacro*{url}{\printfield[url]{urlraw}}

\input{details/masterspecialties.def}
\input{details/barchelorspecialties.def}
\input{details/specialistspecialties.def}

\ExplSyntaxOn

\NewDocumentCommand{\decodespecialty}{m}{
	\decode_barchelor_specialty:Nn \l_tmpa_tl {#1}
	\str_if_empty:NT \l_tmpa_tl {
		\decode_master_specialty:Nn \l_tmpa_tl {#1}
		\str_if_empty:NT \l_tmpa_tl {
			\decode_specialist_specialty:Nn \l_tmpa_tl {#1}
			\str_if_empty:NT \l_tmpa_tl {
				\tl_set:Nn \l_tmpa_tl {
					\todo{НЕИЗВЕСТНАЯ\ СПЕЦИАЛЬНОСТЬ\ #1}
				}
			}
		}
	}
	\tl_use:N \l_tmpa_tl
}
\ExplSyntaxOff

\ExplSyntaxOn
\NewDocumentCommand {\authors} {m} {
	\seq_if_empty:NTF {\g_pct_author_name_seq} {} {\ClassError{details/workbookv1}{Authors~of~the~workbook~were~specefied~more~than~once}}
	\clist_set:Nn \l_tmpa_clist {#1}
	\clist_use:Nn \l_tmpa_clist {}
}
\seq_gclear_new:N \g_pct_author_name_seq
\seq_gclear_new:N \g_pct_author_degree_seq
\seq_gclear_new:N \g_pct_author_employment_seq
\seq_gclear_new:N \g_pct_author_affiliation_seq
\cs_new_protected:Nn \pct_normalize_affiliation:N {\regex_replace_once:nnN {^([^,]+),\ *([\w\-А-Яа-я]+)$}{\1\ \(\2\)} #1}
\DeclareDocumentCommand{\author} {m m O{Дальневосточный~федеральный~университет~(Владивосток)} m} {
	\seq_put_right:Nn \g_pct_author_degree_seq{#1}
	\seq_put_right:Nn \g_pct_author_employment_seq{#2}
	\tl_set:Nn \l_tmpa_tl {#3}
	\pct_normalize_affiliation:N \l_tmpa_tl
	\seq_put_right:NV \g_pct_author_affiliation_seq \l_tmpa_tl
	\seq_put_right:Nn \g_pct_author_name_seq {#4}
}
\NewDocumentCommand{\theauthorname}{O{1}} {
	\seq_item:Nn \g_pct_author_name_seq {#1}
}
\NewDocumentCommand{\theauthordegree}{O{1}} {
	\seq_item:Nn \g_pct_author_degree_seq {#1}
}
\NewDocumentCommand{\theauthoremployment} {O{1}} {
	\seq_item:Nn \g_pct_author_employment_seq{#1}
}
\tl_gclear_new:N \g_pct_institute_tl
\NewDocumentCommand{\institute}{m}{\tl_gset:Nn \g_pct_institute_tl {#1}}
\NewDocumentCommand{\theinstitute}{} {\tl_use:N \g_pct_institute_tl}
\tl_gclear_new:N \g_pct_udc_tl
\NewDocumentCommand{\udc}{m}{\tl_gset:Nn \g_pct_udc_tl {#1}}
\NewDocumentCommand{\theudc}{}{\tl_use:N \g_pct_udc_tl}
\tl_gclear_new:N \g_pct_bbc_tl
\NewDocumentCommand{\bbc}{m}{\tl_gset:Nn \g_pct_bbc_tl {#1}}
\NewDocumentCommand{\thebbc}{}{\tl_use:N \g_pct_bbc_tl}
\tl_gclear_new:N \g_pct_amark_tl
\NewDocumentCommand{\amark}{m}{\tl_gset:Nn \g_pct_amark_tl {#1}}
\NewDocumentCommand{\theamark}{}{\tl_use:N \g_pct_amark_tl}
\tl_gclear_new:N \g_pct_isbn_tl
\NewDocumentCommand{\isbn}{m}{\tl_gset:Nn \g_pct_isbn_tl {#1}}
\NewDocumentCommand{\theisbn}{}{\tl_use:N \g_pct_isbn_tl}
\tl_gclear_new:N \g_pct_copyrighttext_tl
\NewDocumentCommand{\copyrighttext}{m}{\tl_gset:Nn \g_pct_copyrighttext_tl {#1}}
\NewDocumentCommand{\thecopyrighttext}{}{\tl_use:N \g_pct_copyrighttext_tl}
\tl_new:N \g_pct_location_tl
\tl_gset:Nn \g_pct_location_tl {Владивосток}
\NewDocumentCommand{\location}{m}{\tl_gset:Nn \g_pct_location_tl {#1}}
\NewDocumentCommand{\thelocation}{}{\tl_use:N \g_pct_location_tl}
\NewDocumentCommand{\theyear}{}{\the\year}
\tl_gclear_new:N \g_pct_title_tl
\RenewDocumentCommand{\title}{m}{\tl_gset:Nn \g_pct_title_tl {#1} \pct_correct_quoting:N \g_pct_title_tl}
\NewExpandableDocumentCommand{\thetitle}{}{\tl_use:N \g_pct_title_tl}
\tl_gclear_new:N \g_pct_subtitle_tl
\NewDocumentCommand{\subtitle}{m}{\tl_gset:Nn \g_pct_subtitle_tl {#1} \pct_correct_quoting:N \g_pct_subtitle_tl}
\NewDocumentCommand{\thesubtitle}{}{\tl_use:N \g_pct_subtitle_tl}
\tl_gclear_new:N \g_pct_audience_tl
\NewDocumentCommand{\audience}{m}{\tl_gset:Nn \g_pct_audience_tl {#1} \pct_correct_quoting:N \g_pct_audience_tl}
\NewDocumentCommand{\theaudience}{}{\tl_use:N \g_pct_audience_tl}
\tl_gclear_new:N \g_pct_issue_tl
\NewDocumentCommand{\issue}{m}{\tl_gset:Nn \g_pct_issue_tl {#1}}
\NewDocumentCommand{\theissue}{}{\tl_use:N \g_pct_issue_tl}
\tl_gclear_new:N \g_pct_issuetype_tl
\NewDocumentCommand{\issuetype}{m}{\tl_gset:Nx \g_pct_issuetype_tl {#1}}
\NewDocumentCommand{\theissuetype}{}{\tl_use:N \g_pct_issuetype_tl}
\tl_gclear_new:N \g_pct_recommendation_tl
\NewDocumentCommand{\recommendation}{m}{\tl_gset:Nn \g_pct_recommendation_tl {#1}}
\NewDocumentCommand{\therecommendation}{}{\tl_use:N \g_pct_recommendation_tl}
\tl_gclear_new:N \g_pct_sysreq_tl
\NewDocumentCommand{\systemrequirements}{m}{\tl_gset:Nn \g_pct_sysreq_tl {#1}}
\NewDocumentCommand{\thesystemrequirements}{}{\tl_use:N \g_pct_sysreq_tl}
\tl_gclear_new:N \g_pct_pubdate_tl
\NewDocumentCommand{\publicationdate}{m}{\tl_gset:Nn \g_pct_pubdate_tl {#1}}
\NewDocumentCommand{\thepublicationdate}{}{\tl_use:N \g_pct_pubdate_tl}
\tl_gclear_new:N \g_pct_puburl_tl
\NewDocumentCommand{\publicationurl}{m}{\tl_gset:Nn \g_pct_puburl_tl {#1}}
\NewDocumentCommand{\thepublicationurl}{}{
	\tl_set:Ne \l_tmpa_tl {\tl_to_str:N \g_pct_puburl_tl}
	\regex_replace_all:nnN {(.)} {\c{allowbreak}\1} \l_tmpa_tl
	\special{pdf:bann<</Subtype/Link/BS<</Type/Border/W\ 0.0 /S/S>>/A<</S/URI/URI(\tl_to_str:N \g_pct_puburl_tl)>>>>}\l_tmpa_tl\special{pdf:eann}
}
\tl_gclear_new:N \g_pct_pubformat_tl
\NewDocumentCommand{\publicationformat}{m}{\tl_gset:Nn \g_pct_pubformat_tl {#1}}
\NewDocumentCommand{\thepublicationformat}{}{\tl_use:N \g_pct_pubformat_tl}
\NewDocumentEnvironment{annotation} {+b}{}{\tl_gset:Nn \g_pct_annotation_tl {#1}}
\seq_new:N \g_pct_keywords_ru_seq
\seq_new:N \g_pct_keywords_en_seq
\NewDocumentEnvironment{keywords}{O{ru}+b}{
	\tl_set:Nx \l_tmpa_tl {#1}
	\str_case_e:nnF {\str_foldcase:V{\l_tmpa_tl}} {
		{\str_foldcase:n{ru}}{\seq_gset_split:Nnn\g_pct_keywords_ru_seq {,} {#2}}
		{\str_foldcase:n{en}}{\seq_gset_split:Nnn\g_pct_keywords_en_seq {,} {#2}}
	}{\ClassError{details/workbook}{Unsupported~keywords~language~\str_foldcase:V \l_tmpa_tl}}
}{}
\NewDocumentEnvironment{publisherblock}{}{}{}
\seq_gclear_new:N \g_pct_pub_info_seq
\NewDocumentEnvironment{publisherinfo}{+b}{
	\seq_gput_right:Nn \g_pct_pub_info_seq {#1}
}{}

\NewDocumentCommand{\publicationinfo}{+m}{
	\seq_gput_right:Nn \g_pct_pub_info_seq {#1}
}

\RenewDocumentCommand{\titlepage}{} {
	\large
	\setlength{\parindent}{\z@}
	\begin{minipage}{\textwidth}
		\sffamily
		\begin{center}
			Дальневосточный~федеральный~университет
			\normalsize\\
			\theinstitute{}
		\end{center}
	\end{minipage}
	\vspace{7em}
	\par
	\begin{minipage}{\textwidth}
	\begin{center}
		\clist_clear:N \l_tmpa_clist
		\seq_map_inline:Nn \g_pct_author_name_seq {
			\clist_put_right:Nn \l_tmpa_clist {\@fullnameinitialslastname{##1}}
		}
		\LARGE
		\clist_use:Nn \l_tmpa_clist {,~}\par
		\vspace{3em}
		\wb@fontsize{\titlefontsize}
		\bfseries\text_uppercase:n {\thetitle}\normalfont\par
		\tl_if_empty:NF \g_pct_subtitle_tl {\wb@fontsize{\subtitlefontsize}\bfseries\vspace{1.5em}\text_uppercase:n{\g_pct_subtitle_tl}\normalfont\par}
		\tl_if_empty:NF \g_pct_audience_tl {\wb@fontsize{\audiencefontsize}\vspace{1.5em}\tl_use:N \g_pct_audience_tl\par}
		\tl_if_empty:NF \g_pct_issue_tl {\wb@fontsize{\issuefontsize}\vspace{1.5em}\tl_use:N \g_pct_issue_tl\par}
		\tl_if_empty:NF \g_pct_issuetype_tl {\wb@fontsize{\issuetypefontsize}\vspace{1em}\large\tl_use:N \g_pct_issuetype_tl\par}
		\tl_if_empty:NF \g_pct_recommendation_tl {\wb@fontsize{\recommendationfontsize}\itshape\vspace{1.5em}Рекомендовано\par\tl_use:N \g_pct_recommendation_tl\normalfont\par}
		\ifdraught
			\vspace{1ex}
			\small
			\int_compare:nNnTF {\seq_count:N \g_pct_author_name_seq} = {1} {
				Я\ подтверждаю
			}{
				Мы\ подтверждаем
			}
			,\ что\ эта\ работа\ является\ оригинальной\ и\ не\ была\ опубликована\ в\ другом\ месте.\ В\ настоящее\ время\ она\ не\ рассматривается\ для\ публикации\ в\ каком-либо\ другом\ издательстве.\\
			\vspace{1em}
			\clist_clear:N \l_tmpa_clist
			\def\pct@tikzline{\tikz\draw[overlay](0, -1pt) -- +(3cm, 0);\hspace{3cm}}
			\seq_map_inline:Nn \g_pct_author_name_seq {
				\clist_put_right:Nn \l_tmpa_clist {\pct@tikzline\nobreakspace/\ \@fullnameinitialslastname{##1}\ /}
			}
			\clist_use:Nn \l_tmpa_clist {,\par\vspace{1ex}}.
		\fi
	\end{center}\end{minipage}
	\vspace*{\fill}
	\begin{center}
		\sffamily\normalsize
		\includegraphics[width=1.8cm, height=2.5cm]{details/leo-logo}\par
		\thelocation\par
		Издательство\ Дальневосточного\ федерального\ университета\par
		\the\year\par
		\normalfont\normalsize
	\end{center}
	\clearpage
}

\NewDocumentEnvironment{copyrightpage}{}{
	\fontsize{10pt}{11pt}\selectfont\setlength{\parindent}{\z@}\sffamily
	\begin{minipage}{\textwidth}
		УДК\ \theudc{}\\
		ББК\ \thebbc{}\\
		\hspace*{\widthof{ББК~}}\theamark
	\end{minipage}
	\par\small
}{
	\vspace{1em}\normalfont\setlength{\parindent}{\pct@parindent}
	\begin{center}
		\int_compare:nNnTF {\seq_count:N \g_pct_author_name_seq} < {2} {
			\textit{Автор}\par
			\textbf{\seq_item:Nn \g_pct_author_name_seq {1}}~---~
			\clist_clear:N \l_tmpa_clist
			\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_pct_author_degree_seq {1}}
			\tl_if_empty:NF \l_tmpa_tl {
				\abbreviate_degree_short:N \l_tmpa_tl
				\clist_put_right:Nx \l_tmpa_clist \l_tmpa_tl
			}
			\clist_put_right:Nx \l_tmpa_clist {\seq_item:Nn \g_pct_author_employment_seq {1}}
			\clist_use:Nn \l_tmpa_clist {,~}\par
			\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_pct_author_affiliation_seq {1}}
			\regex_replace_once:nnNF {^([^\(]*)\(([\w\-А-Яа-я]+)\)$} {\c{textit}{\1}\ \(\2\)} \l_tmpa_tl {
				\ClassWarning{details/workbook1}{Unrecognized~author~affiliation~pattern}
			}
			\tl_use:N \l_tmpa_tl\par
		}{
			\textit{Авторы}\par
			\seq_set_eq:NN \l_tmpa_seq \g_pct_author_affiliation_seq
			\seq_remove_duplicates:N \l_tmpa_seq
			\int_compare:nNnTF {\seq_count:N \l_tmpa_seq} = {1} {
				\int_set:Nn \l_tmpa_int {1}
				\int_set:Nn \l_tmpb_int {\seq_count:N \g_pct_author_name_seq}
				\int_do_while:nn {\l_tmpa_int <= \l_tmpb_int} {
					\textbf{\seq_item:Nn \g_pct_author_name_seq {\int_use:N \l_tmpa_int}}~---~
					\clist_clear:N \l_tmpa_clist
					\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_pct_author_degree_seq {\int_use:N \l_tmpa_int}}
					\tl_if_empty:NF \l_tmpa_tl {
						\abbreviate_degree_short:N \l_tmpa_tl
						\clist_put_right:Nx \l_tmpa_clist \l_tmpa_tl
					}
					\clist_put_right:Nx \l_tmpa_clist {\seq_item:Nn \g_pct_author_employment_seq {\int_use:N \l_tmpa_int}}
					\clist_use:Nn \l_tmpa_clist {,~}\par
					\int_incr:N \l_tmpa_int
				}
				\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_pct_author_affiliation_seq {1}}
				\regex_replace_once:nnNF {^([^\(]*)\(([\w\-А-Яа-я]+)\)$} {\c{textit}{\1}\ \(\2\)} \l_tmpa_tl {
					\ClassWarning{details/workbook1}{Unrecognized~author~affiliation~pattern}
				}
				\tl_use:N \l_tmpa_tl\par
			}{
				\int_set:Nn \l_tmpa_int {1}
				\int_set:Nn \l_tmpb_int {\seq_count:N \g_pct_author_name_seq}
				\int_do_while:nn {\l_tmpa_int <= \l_tmpb_int} {
					\textbf{\seq_item:Nn \g_pct_author_name_seq {\int_use:N \l_tmpa_int}}~---~
					\clist_clear:N \l_tmpa_clist
					\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_pct_author_degree_seq {\int_use:N \l_tmpa_int}}
					\tl_if_empty:NF \l_tmpa_tl {
						\abbreviate_degree_short:N \l_tmpa_tl
						\clist_put_right:Nx \l_tmpa_clist \l_tmpa_tl
					}
					\clist_put_right:Nx \l_tmpa_clist {\seq_item:Nn \g_pct_author_employment_seq {\int_use:N \l_tmpa_int}}
					\clist_use:Nn \l_tmpa_clist {,~}\par
					\tl_set:Nx \l_tmpa_tl {\seq_item:Nn \g_pct_author_affiliation_seq {\int_use:N \l_tmpa_int}}
					\regex_replace_once:nnNF {^([^\(]*)\(([\w\-А-Яа-я]+)\)$} {\c{textit}{\1}\ \(\2\)} \l_tmpa_tl {
						\ClassWarning{details/workbook1}{Unrecognized~author~affiliation~pattern}
					}
					\tl_use:N \l_tmpa_tl\par
					\int_incr:N \l_tmpa_int
				}
			}
		}
	\end{center}
	\clist_clear:N \l_tmpa_clist
	\seq_map_inline:Nn \g_pct_author_name_seq {
		\tl_set:Nn \l_tmpa_tl {##1}
		\fullname_to_lastname_initials:N \l_tmpa_tl
		\clist_put_right:NV \l_tmpa_clist \l_tmpa_tl
	}
	\setlength{\parindent}{\pct@parindent}\par\textbf{\clist_use:Nn \l_tmpa_clist {,\ }}\ 
	\thetitle{}
	\tl_if_empty:NF \g_pct_subtitle_tl {:~\tl_use:N \g_pct_subtitle_tl}
	\tl_if_empty:NF \g_pct_audience_tl {
		\tl_set_eq:NN \l_tmpa_tl \g_pct_audience_tl
		\regex_replace_all:nnN{\ *\c{\\}\ *}{\ } \l_tmpa_tl
		:~\tl_use:N \l_tmpa_tl
	}
	\tl_if_empty:NF \g_pct_issuetype_tl {
		\tl_set_eq:NN \l_tmpa_tl \g_pct_issuetype_tl
		\regex_replace_all:nnN{.*?\c{\\}(.*)$}{\1} \l_tmpa_tl
		:~\tl_use:N \l_tmpa_tl
	}
	\ /~Политехнический~институт~ДВФУ.
	\tl_if_empty:NF \g_pct_issue_tl {\ --\ \tl_set:No \l_tmpa_tl \g_pct_issue_tl \abbreviate_issue:N\l_tmpa_tl \tl_use:N \l_tmpa_tl}
	\ --\ \tl_set:No \l_tmpa_tl {\g_pct_location_tl :\ Изд-во\ Дальневост.\ федеральн.\ ун-та}
	\tl_use:N \l_tmpa_tl,\ \the\year{}.
	\regex_set:Nn \l_tmpa_regex {^(.*)\.$}
	\ --\tl_if_empty:NF \g_pct_pubformat_tl{\ \regex_replace_once:NnN \l_tmpa_regex {\1} \g_pct_pubformat_tl \g_pct_pubformat_tl.}\ [\totalpagecount*\ с.].
	\tl_if_empty:NF \g_pct_sysreq_tl {\ --\ Систем.\ требования:\ \regex_replace_once:NnN \l_tmpa_regex {\1} \g_pct_sysreq_tl \g_pct_sysreq_tl.}
	\tl_if_empty:NF \g_pct_puburl_tl {
		\regex_replace_once:NnN \l_tmpa_regex {\1} \g_pct_puburl_tl
		\tl_set:Ne \l_tmpa_tl {\tl_to_str:N \g_pct_puburl_tl}
		\regex_replace_all:nnN {(.)} {\c{allowbreak}\1} \l_tmpa_tl
		\ --\ URL:\ \special{pdf:bann<</Subtype/Link/BS<</Type/Border/W\ 0.0 /S/S>>/A<</S/URI/URI(\tl_to_str:N \g_pct_puburl_tl)>>>>}\l_tmpa_tl.\special{pdf:eann}
	}
	\tl_if_empty:NF \g_pct_pubdate_tl {\ --\ Дата\ публикации:\ \regex_replace_once:NnN \l_tmpa_regex {\1} \g_pct_pubdate_tl \g_pct_pubdate_tl.}
	\ --\ ISBN\ \tl_use:N \g_pct_isbn_tl.
	\ --\ Текст:\ электронный.\par
	\vspace{1em}\tl_use:N \g_pct_annotation_tl\par
	\seq_if_empty:NF \g_pct_keywords_ru_seq {\itshape Ключевые\ слова:\normalfont\ \seq_use:Nn \g_pct_keywords_ru_seq {,\ }}\par
	\seq_if_empty:NF \g_pct_keywords_en_seq {\itshape Keywords:\normalfont\ \seq_use:Nn \g_pct_keywords_en_seq {,\ }}\par
	\vspace*{\fill}
	\seq_if_empty:NF \g_pct_pub_info_seq {
		\begin{center}
			\fontsize{10pt}{13.5pt}\selectfont
			\let\oldnl\\
			\def\\{\oldnl\relax}
			\seq_use:Nn \g_pct_pub_info_seq {\par\vspace*{\fill}}
			\let\\\oldnl
		\end{center}
		\par\vspace*{\fill}
	}
	\fontsize{10pt}{13pt}\selectfont
	\setlength{\parindent}{\z@}
	\hspace{\fill}Защищено\ от\ копирования \\
	ISBN\ \tl_use:N \g_pct_isbn_tl \hspace{\fill}\copyright{}\ ФГАОУ\ ВО\ \quot{ДВФУ},\ \the\year
	\clearpage
	\normalfont\normalsize
}

\ExplSyntaxOff
\let\totalprintlists@base\totalprintlists
\renewcommand{\totalprintlists}{\@ifstar{\totalprintlists@base*{250}{2159}}{\totalprintlists@base{250}{2159}}}
\input{details/contractions.def}

\AtBeginDocument{%
	\parskip\pct@parskip
	\parindent\pct@parindent
}
